# Kurator

Used by Kuterun to configure testing environment and to collect testing results.

## Build
```
make docker
```

## Lint example
```
golangci-lint run internal/tests_kurator --enable-all --disable wsl --disable nlreturn
```

